import { createSlice } from '@reduxjs/toolkit';

const homeSlice = createSlice({
    name: 'home',
    initialState: {
        data: [],
        loading: true,
        favorites: [],
        query: '',
        language: '',
        category: '',
    },
    reducers: {
        fetchSuccess: (state, action) => {
            state.data = action.payload;
            state.loading = false;
        },
        updateCategory: (state, action) => {
            state.category = action.payload;
        },
        updateQuery: (state, action) => {
            state.query = action.payload;
        },
        updateLanguage: (state, action) => {
            state.language = action.payload;
        },
        toggleFavorites: (state, action) => {
            state.favorites = action.payload;
        },
        setFavorites: (state, action) => {
            state.favorites = action.payload; // Overwrite favorites with new data
        },        
        removeFromFavorites(state, action) {
            // state.favorites.splice(action.payload, 1); // Remove a favorite by index
            state.favorites = state.favorites.filter((_, index) => index !== action.payload);
            localStorage.setItem('favorites', JSON.stringify(state.favorites));
        }
    },
});

export const { fetchSuccess, updateCategory, updateQuery, updateLanguage, toggleFavorites,setFavorites,removeFromFavorites  } = homeSlice.actions;
export default homeSlice.reducer;
