import React from 'react'
import {useState} from 'react'

const Form = () => {
    const [name,setName] = useState("");
    const [tnc,setTnc] = useState(false);
    const [interest,setInterest] = useState("");
    function getFromData(e){
        console.log(name,tnc,interest)
        e.preventDefault()
    }
  return (
    <div>
      <h1 className='input'>Form handling</h1>
      <form onSubmit={getFromData}>
        <input type="text" placeholder="Enter Name" onChange={(e)=>setName(e.target.value)}/>
        <br/><br/>
        <select onChange={(e)=>setInterest(e.target.value)}>
            <option >Select Option</option>
            <option value="fruits">Fruits</option>
            <option value="vegetable">Vegetable</option>
        </select>
        <br/><br/>
        <input type="checkbox" onChange={(e)=>setTnc(e.target.checked)} /><span className='input'>Accept term and condition</span>
        <br/><br/>
        <button type="submit">Submit</button>
        <button type="submit">Clear</button>
      </form>
    </div>
  )
}

export default Form
