import React,{useState} from 'react'
import Navigation from './Navigation'
import {BrowserRouter as Router,Routes,Route} from 'react-router-dom'
import Home from './Home'
import Favorites from './Favorites'
import './style.css'
const NewsApp = () => {
  const [category,setCategory] = useState(null);  
  const [theme,setTheme] = useState('light');
  const toggleTheme = () => {
    setTheme(theme === 'light' ? 'dark' : 'light');
  }
  return (
    <div className={`app-${theme}`}>
      {/* <Navigation/>   */}
       <Router>
      <Navigation toggleTheme={toggleTheme} theme={theme}/>      
        <Routes>            
            <Route path="/" element={<Home setCategory={setCategory} category="general" theme={theme}/> }/>
            <Route path="/business" element={<Home setCategory={setCategory} category="business" theme={theme}/> }/>
            <Route path="/entertainment" element={<Home setCategory={setCategory} category="entertainment"/> }/> 
            <Route path="/health" element={<Home setCategory={setCategory} category="health" theme={theme}/> }/>  
            <Route path="/sports" element={<Home setCategory={setCategory} category="sports" theme={theme}/> }/>
            <Route path="/science" element={<Home setCategory={setCategory} category="science" theme={theme}/> }/>
            <Route path="/technology" element={<Home setCategory={setCategory} category="technology" theme={theme}/> }/>
            <Route path="/search/:query" element={<Home />} />
            <Route path="/language/:language" element={<Home />} />
            <Route path="/favorites" element={<Favorites theme={theme}/>} />
        </Routes>
      </Router> 
    </div>
  )
}

export default NewsApp
