import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import './navbar.css'
const Navigation = ({ toggleTheme, theme }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [selectedLanguage, setSelectedLanguage] = useState('');
  const navigate = useNavigate();
  const handleSearchClick = (e) => {
    e.preventDefault();
    navigate(`/search/${searchQuery}`);
  }
  const handleSelectedLanguage = (e) => {
    e.preventDefault();
    const selectedLanguage = e.target.value;
    setSelectedLanguage(selectedLanguage);
    navigate(`/language/${selectedLanguage}`);
  }

  return (
    <div>
      <nav className={`navbar navbar-expand-lg ${theme === 'light' ? 'navbar-light' : 'navbar-dark'} bg-${theme}`}>
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Navbar</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item"><Link to="/" className="nav-link">General</Link></li>
              <li className="nav-item"><Link to="/business" className="nav-link">Business</Link></li>
              <li className="nav-item"><Link to="/entertainment" className="nav-link">Entertainment</Link></li>
              <li className="nav-item"><Link to="/health" className="nav-link">Health</Link></li>
              <li className="nav-item"><Link to="/science" className="nav-link">Science</Link></li>
              <li className="nav-item"><Link to="/sports" className="nav-link">Sports</Link></li>
              <li className="nav-item"><Link to="/technology" className="nav-link">Technology</Link></li>
              <li className='nav-item'><Link to="/favorites" className='nav-link'>Favorites</Link></li>
              <div className="collapse navbar-collapse navbar-dropdown" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item dropdown">
                    <select className="form-select" onChange={handleSelectedLanguage} value={selectedLanguage}>
                      <option value="">Select Language</option>
                      <option value="hi">Hindi</option>
                      <option value="en">English</option>
                    </select>
                  </li>
                </ul>
              </div>
            </ul>
            <form className="d-flex" onSubmit={handleSearchClick}>
              <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" onChange={(e) => setSearchQuery(e.target.value)} />
              <button className="btn btn-outline-success" type="submit">Search</button>
            </form>
            {/* <button onClick={toggleTheme}>Toggle</button> */}
            <input type="checkbox" id="switch" onClick={toggleTheme} /><label for="switch">Toggle</label>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Navigation
