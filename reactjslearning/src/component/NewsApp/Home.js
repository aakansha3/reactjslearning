// import React, { useEffect, useState } from 'react';
// import { useParams } from 'react-router-dom';
// import './style.css';
// import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
// import 'react-loading-skeleton/dist/skeleton.css'
// const Home = ({category}) => {
//   const [data, setData] = useState([]);
//   const [loading, setLoading] = useState(true);
//   const {query} = useParams();
//   // https://newsapi.org/v2/everything?q=aliabhatt&apiKey=cb25bdbf5fe942f697591fc2950f37d8
//   // const url = category ? `https://newsapi.org/v2/top-headlines?country=in&category=${category}&apikey=cb25bdbf5fe942f697591fc2950f37d8` : `https://newsapi.org/v2/top-headlines?country=in&apikey=cb25bdbf5fe942f697591fc2950f37d8`;
//   let baseApiUrl = `https://newsapi.org/v2/`;
//   if (category) {
//     baseApiUrl += `top-headlines?country=in&category=${category}&`;
//   } else if (query) {
//     baseApiUrl += `everything?q=${query}&`;
//   }
//   const apikey = `apikey=a9f99ea76ed4470ea4b64990461ccd79`; // Make sure to replace YOUR_API_KEY with your actual NewsAPI key
//   const url = `${baseApiUrl}${apikey}`;  
//   const fetchInfo = () => {
//     setLoading(true);
//     return fetch(url)
//       .then((res) => res.json())
//       .then((d) => {
//         setData(d);
//         setLoading(false); // Set loading to false once data is fetched
//       });
//   };
//   useEffect(() => {
//     fetchInfo();
//   }, [category,query]);

//   useEffect(() => {
//     console.log('Data fetched:', data);
//   }, [data]);

//   return (
//     <div className="container">
//       {data && data.articles && data.articles.map((article, index) => (
//         <div className="card" style={{ width: '18rem' }} key={index}>
//           <img src={article.urlToImage} className="card-img-top" alt={article.title} />
//           <div className="card-body">
//             <h5 className="card-title">{article.title || <Skeleton />}</h5>
//             <p className="card-text">{article.content || <Skeleton />}</p>
//           </div>
//           <ul className="list-group list-group-flush">
//             <li className="list-group-item">Author:  {article.author || <Skeleton />}</li>            
//           </ul>
//           <div className="card-body">
//             <a href={article.url || <Skeleton />} className="card-link">Read more</a>
//           </div>
//         </div>
//       ))}
//     </div>
//   );
// };

// export default Home;
import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch,useSelector } from 'react-redux';
import {
  fetchSuccess,
  updateCategory,
  updateQuery,
  updateLanguage,
  toggleFavorites
} from '../../features/homeSlice'; 
import './style.css';
import Skeleton, { SkeletonTheme }  from 'react-loading-skeleton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons';
import { faHeart as fasHeart } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import ZustandStore from '../../app/zustandStore';

const Home = ({ category,theme }) => {
  const { data, loading, favorites, query, language, updateCategory, updateQuery, updateLanguage, toggleFavorites, fetchSuccess } = ZustandStore(); // Use Zustand hooks
 
  const { query: routeQuery, language: routeLanguage} = useParams();

  useEffect(()=>{
    if(category){      
      updateCategory(category);
    }
    else if(routeQuery){
     updateQuery(routeQuery);
    }
    else if(routeLanguage){
      updateLanguage(routeLanguage);
    }
  },[routeQuery, routeLanguage, category]);

  useEffect(() => {
    let baseApiUrl = `https://newsapi.org/v2/`;
        if (category) {
          baseApiUrl += `top-headlines?country=in&category=${category}&`;
        } else if (query) {
          baseApiUrl += `everything?q=${query}&`;
        }
        else if (language) {
          baseApiUrl += `everything?q=modi&language=${language}&`;
        }
        else{
          baseApiUrl += `top-headlines?country=in&category=general&`
        }
        const apikey = `apikey=a9f99ea76ed4470ea4b64990461ccd79`;
        const url = `${baseApiUrl}${apikey}`; 
    axios.get(url).then((response)=>{
      const responseData = response.data;
      fetchSuccess(responseData.articles);
    })
  },[category,query,language]);
     // Specify dependencies here


  
  useEffect(()=>{
    console.log('Data state:', data);
  },[data]);

  const AddingToFavorite = (article) => {
    console.log('fav', article); 
    toggleFavorites(article);  
  };
  
  
  

  return (
    <div className={`container ${theme === 'dark' ? 'dark-theme' : ''}`}>
      <SkeletonTheme baseColor="black" highlightColor="white"> {/* Move this line here */}
        {loading ? (
          Array.from({ length: 8 }).map((_, index) => (
            <div className={`card ${theme === 'dark' ? 'dark-card' : ''}`} style={{ width: '18rem' }} key={index}>
              <Skeleton height={200} width={'100%'}/>
              <div className="card-body">
                <h5 className="card-title">
                  <Skeleton count={3}/>
                </h5>
                <p className="card-text">
                  <Skeleton count={3} />
                </p>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <Skeleton />
                </li>
              </ul>
              <div className="card-body">
                <Skeleton width={100} />
              </div>
            </div>
          ))
        ) : (
          data.map((article, index) => (
            <div className={`card ${theme === 'dark' ? 'dark-card' : ''}`} style={{ width: '18rem' }} key={index}>
              <img src={article.urlToImage} className="card-img-top" alt={article.title} />
              <div className="card-body">
                <h5 className="card-title">{article.title ? article.title.slice(0, 60) : 'Untitled'}</h5>
                <p className="card-text">{article.description ? article.description.slice(0, 120) : 'Description not available'}</p>
              </div>
              <p className='author'>Author: {article.author}</p>
              <div className="card-body">
                <a href={article.url} className="card-link">
                  Read more
                </a>
                <button className="fav-button" onClick={() => AddingToFavorite(article)} aria-label={favorites.some((fav) => fav.title === article.title) ? "Remove from favorites" : "Add to favorites"}>
                  <FontAwesomeIcon icon={favorites.some((fav) => fav.title === article.title) ? fasHeart : farHeart} />
                </button>
              </div>
            </div>
          ))
        )}
      </SkeletonTheme> {/* Move this line here */}
    </div>
  );
};

export default Home;

