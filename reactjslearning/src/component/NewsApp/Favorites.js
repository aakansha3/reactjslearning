import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as fasHeart } from '@fortawesome/free-solid-svg-icons';
import ZustandStore from '../../app/zustandStore';
// const { removeFromFavorites } = ZustandStore.getState();
const Favorites = ({ theme }) => {   
    const favorites = ZustandStore((state) => state.favorites);  
    const { removeFromFavorites } = ZustandStore();
    useEffect(() => {
        const storedFavorites = localStorage.getItem('favorites');
        if (storedFavorites) {
            ZustandStore.setState({ favorites: JSON.parse(storedFavorites) });
        }
    }, []);

    const handleRemoveFromFavorites = (index) => {        
        const articleToRemove = favorites[index];
        console.log(articleToRemove);
        removeFromFavorites(articleToRemove);
    };

    return (
        <div className={`container ${theme === 'dark' ? 'dark-theme' : ''}`}>
            {favorites && favorites.length > 0 ? (
                favorites.map((article, index) => (
                    <div className={`card ${theme === 'dark' ? 'dark-card' : ''}`} style={{ width: '18rem' }} key={index}>
                        <img src={article.urlToImage} className="card-img-top" alt={article.title} />
                        <div className="card-body">
                            <h5 className="card-title">{article.title ? article.title.slice(0, 60) : 'Untitled'}</h5>
                            <p className="card-text">{article.description ? article.description.slice(0, 120) : 'Description not available'}</p>
                        </div>
                        <p className='author'>Author: {article.author}</p>
                        <div className="card-body">
                            <a href={article.url} className="card-link">
                                Read more
                            </a>
                            <button className="fav-button" onClick={() => handleRemoveFromFavorites(index)}>
                                <FontAwesomeIcon icon={fasHeart} />
                            </button>
                        </div>
                    </div>
                ))
            ) : (
                // Display a message if there are no favorites
                <p>No favorites saved</p>
            )}
        </div>
    );
}

export default Favorites;
