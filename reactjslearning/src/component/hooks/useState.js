import React, {useState} from "react";
import "./style.css";

const UseState = () => {
    const initialData = 0;
    const [MyNum,setMyNum] = useState(initialData);
    return (
        <>
        <div className="center_div">
            <p>{MyNum}</p>
            <div class="button2" onClick={()=>setMyNum(MyNum + 1)}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                INCR
            </div>
            
            {/* <div class="button2" onClick={()=>setMyNum(MyNum - 1)}> */}
            <div class="button2" onClick={()=>(MyNum > 0 ? setMyNum(MyNum - 1) : setMyNum(0))}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                DECR
            </div>
        </div>
        </>
    );
};
export default UseState;