import React from 'react'
import UseRedux from './useRedux'
import Navbar from './navbar'
const Mainredux = () => {
  return (
   <>
   <Navbar/>
   <UseRedux/>
   </>
  )
}

export default Mainredux
