import React,{useState,useEffect} from 'react'
import './style.css'

const UseEffect = () => {
    const[count,setCount] = useState(0);
    useEffect(()=>{
        // document.body.style.background = "white";
        // document.title = `Chats ${count}`;
        // alert("Hello");     
        console.log(count);
    },[count]);//The dependency array is important that it will tell that when to effect appear or when this action perform 
    //In array count is pass it means that this action will be performed whenever state of count will change
    //unsubscribing,fetching data
  return (
    <div>
      <div class="button2" onClick={()=>setCount(count+1)}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                INCR
            </div>
    </div>
  )
}

export default UseEffect;
// Components will render on intially when page refresh,then state and props change
