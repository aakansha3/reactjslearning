// UseMemo is used to increase performance of website
// import React, { useState } from 'react'

// const UseMemo = () => {
//     const [add,setAdd] = useState(0);
//     const [minus,setMinus] = useState(100);
//     function multiply(){
//         console.log("************");
//         return add*10;
//     }
//   return (
//     <div>
//         <h1 className='input'>{multiply()}</h1>
//         <h1 className='input'>{add}</h1>
//       <button onClick={()=>{setAdd(add+1)}}>Addition</button>
//       <h1 className='input'>{minus}</h1>
//       <button onClick={()=>{setMinus(minus-1)}}>Substraction</button>
//     </div>
//   )
// }

// export default UseMemo
//substraction button doesnot relate to multiply function then also clicking on substraction button it will call multiply function this will impact to performance

import React, { useState,useMemo } from 'react'

const UseMemo = () => {
    const [add,setAdd] = useState(0);
    const [minus,setMinus] = useState(100);
    const multiplication = useMemo(function multiply(){
        console.log("************");
        return add*10;
    },[add])
    
  return (
    <div>
        <h1 className='input'>{multiplication}</h1>
        <h1 className='input'>{add}</h1>
      <button onClick={()=>{setAdd(add+1)}}>Addition</button>
      <h1 className='input'>{minus}</h1>
      <button onClick={()=>{setMinus(minus-1)}}>Substraction</button>
    </div>
  )
}
export default UseMemo