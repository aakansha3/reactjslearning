import React,{useState,useEffect,useRef} from 'react'

const UseRef = () => {
    const [inputValue, setInputValue] = useState('');
    const inputElement = useRef();
    const focusInput = () => {
        inputElement.current.focus();
    }
    // const [count,setCount] = useState();
    const count = useRef(0);
    useEffect(()=>{
        // setCount(count + 1);
        count.current = count.current + 1;
    })
  return (
    <div>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <button onClick={focusInput}>FocusInput</button>
       <h1 className='input'>Render Count: {count.current}</h1>
    </div>
  )
}

export default UseRef
