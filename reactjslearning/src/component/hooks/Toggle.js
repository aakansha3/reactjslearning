import React,{useState} from 'react'

const Toggle = () => {    
    const [isVisible,setIsVisible] = useState(false);
    function handleToggleVisibility(){
        setIsVisible(!isVisible);
    }
  return (
    <div>      
      {isVisible && <p className='input'>Toggle me!</p>}
      <button onClick={handleToggleVisibility}>Show Hide</button>
      
    </div>
  )
}

export default Toggle
