import React, { useContext } from 'react'
import {data,data1} from './Parent'
const ChildB = () => {
    const name = useContext(data);
    const gender = useContext(data1);
  return (
    <div>
      <h1 className='input'>{name} & {gender}</h1>
    </div>
  )
}

export default ChildB
