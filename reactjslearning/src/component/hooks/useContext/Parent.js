// using context we can pass data from parent to its specific child
// create,provider,useContext
// Parent Passing data to ChildB
import React, { createContext } from 'react'
import ChildA from './ChildA';

const data = createContext();
const data1 = createContext();

const Parent = () => {
    const name = "Aakansha";
    const gender = "female";
  return (
    <div>
        <data.Provider value={name}>
        <data1.Provider value={gender}>
      <ChildA/>
      </data1.Provider>
      </data.Provider>
    </div>
  )
}

export default Parent
export {data,data1}