import React,{useState} from 'react'

const TextInput = () => {
    const initialData = '';
    const [word,setword] = useState(initialData);
    const handleInputChange = (event) => {
        setword(event.target.value);
    }
  return (
    <div>
      <input type="text" value={word} onInput = {handleInputChange}/>
      <h1 className='input'>{word}</h1>
    </div>
  )
}

export default TextInput;
