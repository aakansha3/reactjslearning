import React from 'react'
import './Navbar.css'


const Navbar = () => {
  return (
    <nav className='w-[1366px] h-[74px] bg-black font-inter flex gap-[492px] py-[18px] px-[123px]'>
        <div className='w-[99px] h-[28px] text-[24px] font-bold text-white leading-[28px]'>Alifreza<span className='font-inter text-[24px] font-bold leading-[28px] text-left text-[#079211]'>.</span></div>
        <div className='w-[529px] h-[38px] gap-[58px]'>
        <div className='w-[383px] h-[17px] gap-[38px] flex text-white'>
            <div className='w-[41px] h-[17px] font-inter font-bold leading-[16.94px] text-[14px] py-[10px]'>Home</div>
            <div className='w-[60px] h-[17px] font-inter font-bold leading-[16.94px] text-[14px] gap-[5px] py-[10px]'>Services</div>
            <div className='w-[37px] h-[17px] font-inter font-bold leading-[16.94px] text-[14px] gap-[5px] py-[10px]'>work</div>
            <div className='w-[62px] h-[17px] font-inter font-bold leading-[16.94px] text-[14px] gap-[5px] py-[10px]'>Aboutus</div>
            <div className='w-[31px] h-[17px] font-inter font-bold leading-[16.94px] text-[14px] gap-[5px] py-[10px]'>Blog</div>
            <div className='w-[88px] h-[38px] border-[1px] border-[#079211] font-inter font-bold font-white leading-[16.94px] text-[14px] gap-[10px] py-[10px] px-[16px]'>Contact</div>
        </div>
        </div>
    </nav> 
  )
}

export default Navbar

