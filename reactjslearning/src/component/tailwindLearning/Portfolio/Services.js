import React from 'react'
import './Navbar.css'
const Services = () => {
    return (
        <>
            <div className='w-[1120px] h-[544px] absolute top-[708px] left-[123px] '>
                <div className='w-[1120px] h-[112px] gap-[303px] flex'>
                    <div className='w-[457px] h-[112px] font-inter text-4xl font-bold leading-[56px] text-left text-white' >7+ years experience
                        working
                    </div>
                    <div className='w-[360px] h-[73px]'>
                        <div className='w-[152px] h-[24px] flex'>
                            <div className='absolute top-[11px]'><img src="line.svg"/></div>
                            <div className='w-[86px] h-[24px] font-inter text-[20px] font-bold leading-[24.2px] text-left text-[#079211] ml-[66px]'>Services</div>
                        </div>
                        <div className='w-[360px] h-[41px] font-inter font-normal text-[16px] leading-[19.36px] text-left text-[#808080]'>Discover the best services I offer, designed to ensure the success of your project.</div>
                    </div>
                </div>
                

                <div className='w-[1120px] h-[342px] flex justify-between mt-[90px]'>
                    <div className='w-[360px] h-[342px] flex-col'>                   
                    <div className='w-[360px] h-[336px] bg-[#313131]'>
                        <div className='w-[50px] h-[47px] relative'>
                            <img src='icon-1.svg' className='absolute top-[68px] left-[25px]' />
                        </div>
                        <div className='w-[310px] h-[107px] gap-[18px] relative top-[128px] ml-[25px]'>
                            <div className='w-full h-[32px] font-inter text-[26px] font-bold leading-[32px] text-left text-white'>Product Design</div>
                            <div className='w-[310px] h-[57px] font-inter text-[16px] text-bold leading-[19.36px] text-left text-[#808080] mt-[18px]'>I offer innovative and attention-grabbing product design services. From initial ideation to implementation.</div>
                        </div>                                                
                    </div>
                    <div className='w-[360px] h-[6px] bg-[#079211]' ></div>
                    </div> 
                    
                    <div className='w-[360px] h-[342px] flex-col'> 
                    <div className='w-[360px] h-[336px] bg-[#313131]'>
                        <div className='w-[50px] h-[47px] relative'>
                            <img src='icon-2.svg' className='absolute top-[68px] left-[25px]' />
                        </div>
                        <div className='w-[310px] h-[107px] gap-[18px] relative top-[128px] ml-[25px]'>
                            <div className='w-full h-[32px] font-inter text-[26px] font-bold leading-[32px] text-left text-white'>Frontend develop</div>
                            <div className='w-[310px] h-[57px] font-inter text-[16px] text-bold leading-[19.36px] text-left text-[#808080] mt-[18px]'>Specialize in responsive and interactive front-end development. With a deep understanding of HTML, CSS, and JS.</div>
                        </div>
                    </div>
                    <div className='w-[360px] h-[6px] bg-[#079211]' ></div>
                    </div>

                    <div className='w-[360px] h-[342px] flex-col'> 
                    <div className='w-[360px] h-[336px] bg-[#313131]'>
                        <div className='w-[50px] h-[47px] relative'>
                            <img src='icon-3.svg' className='absolute top-[68px] left-[25px]' />
                        </div>
                        <div className='w-[310px] h-[107px] gap-[18px] relative top-[128px] ml-[25px]'>
                            <div className='w-full h-[32px] font-inter text-[26px] font-bold leading-[32px] text-left text-white'>Brand strategy</div>
                            <div className='w-[310px] h-[57px] font-inter text-[16px] text-bold leading-[19.36px] text-left text-[#808080] mt-[18px]'>I help design strong and captivating brand strategies. Through thorough research and market analysis.</div>
                        </div>
                    </div>
                    <div className='w-[360px] h-[6px] bg-[#079211]' ></div>
                    </div>
                    
                </div>
            </div>
        </>
    )
}

export default Services







