import React from 'react'

const Contact = () => {
  return (
    <>
    <div className='w-[10px] h-[213px] absolute top-[3520px] bg-[#079211]'></div>
    <div className='w-[1356px] h-[213px] absolute top-[3520px] left-[10px] bg-[#0A0A0F]'>
      <div className='w-[1120px] h-[87px] absolute top-[63px] left-[123px] gap-[411px] flex'>
        <div className='w-[550px] h-[87px] gap-[48px '>
            <div className='w-[486px] h-[46px] font-inter text-[38px] font-bold leading-[45.99px] text-left text-white'>Have any project in mind ?</div>
            <div className='w-[550px] h-[25px] font-inter text-[16px] font-normal leading-[28px] text-left text-[#808080] mt-[16px]'>Feel free to contact me or just say friendly hello!</div>
        </div>
        <div className='w-[550px] h-[87px]'>
            <div className='w-[159px] h-[54px] py-[18px] px-[28px] gap-[10] bg-[#079211] mt-[16.5px]'>
                <div className='w-[103px] h-[18px] font-inter text-[18px] font-bold leading-[18px] text-left text-white'>Contact Me</div>
            </div>
        </div>
      </div>
    </div>
    </>
  )
}

export default Contact





