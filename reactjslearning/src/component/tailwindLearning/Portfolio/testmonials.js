import React from 'react'
import './Navbar.css'
const Testmonials = () => {
  return (
    <>
    <div className='text-white w-[1120px] h-[411px] absolute top-[3006px] left-[123px]'>
      <div className='w-[555px] h-[411px]  gap-[90px]  ml-[283px]'>
        <div className='w-[398px] h-[96px] gap-[16px]  ml-[78.5px]'>
            <div className='w-[191px] h-24px gap-[8px]  ml-[103.5px] flex'>
                <img src='line.svg' className='absolute top-[11.1px]'/>
                <div className='w-[125px] h-[24px] font-inter text-[20px] font-bold leading-[24.2px] text-left text-[#079211]  relative left-[66px]' >Testimonials</div>
            </div>
            <div className='w-[398px] h-[56px] font-inter text-[46px] font-bold leading-[56px] text-center text-white mt-[16px]'>My happy clients</div>
        </div>

        <div className='w-[555px] h-[225px] gap-[48px]  mt-[90px]'>
            <div className='w-[555px] h-[171px] gap-[37px]'>
                <div className='w-[555px] h-[84px] font-inter text-[18px] font-normal leading-[28px] text-center text-[#808080]'>"Wow is all i can say! Alifreza did an absolute fenominal job from start to finish. He created exactly what I wanted and exceeded my expectation. Highly recommend alifreza!"</div>
                <div className='w-[227px] h-[50px] gap-[20px]  mt-[37px] ml-[164px] flex'>
                    <div className='w-[50px] h-[50px]'><img src='monalisa.svg'/></div>
                    <div className='w-[157px] h-[48px] gap-[2px] flex-col '>                        
                        <div className='w-full h-[28px] font-inter text-[20px] font-bold leading-[28px] text-center text-white '>Monalisa Nona </div>
                        <div className='w-[157px] h-[18px] font-inter text-[14px] font-normal leading-[18px] text-center'>Project Manager, Ruber</div>
                    </div>
                </div>
                <div className='w-[34px] h-[6px] gap-[8px] relative left-[260.5px] top-[48px] flex'>
                    <div className='h-[6px] w-[6px]'><img src='Ellipse-13.svg'/></div>
                    <div className='h-[6px] w-[6px]'><img src='Ellipse-14.svg'/></div>
                    <div className='h-[6px] w-[6px]'><img src='Ellipse-14.svg'/></div>
                </div>
            </div>
            
        </div>
      </div>
    </div>
    <div className='w-[1120px] h-[24px] absolute top-[3266px] left-[123px] gap-[1072px] flex '>
        <div className='w-[24px] h-[24px] bg-black'><img src='Vector3.svg' className='absolute top-[3.73px] left-[7.5px]'/></div>
        <div className='w-[24px] h-[24px] bg-black'><img src='Vector4.svg' className='relative top-[3.77px] left-[7.52px]' /></div>
    </div>
    </>
    
  )
}

export default Testmonials


