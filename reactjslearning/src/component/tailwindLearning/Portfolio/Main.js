import React from 'react'
import Navbar from './Navbar'
import Hero from './Hero'
import Services from './Services'
import Work from './Work'
import Testmonials from './testmonials'
import Contact from './Contact'
import Footer from './Footer'
const Main = () => {
  return (
    <>
    <Navbar/>
    <Hero/>
    <Services/>
    <Work/>
    <Testmonials/>
    <Contact/>
    <Footer/>
    </>   
  )
}

export default Main
