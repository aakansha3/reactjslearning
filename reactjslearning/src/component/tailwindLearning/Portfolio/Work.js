import React from 'react'
import './Navbar.css'
const Work = () => {
  return (
    <>
    <div className='w-[1366px] h-[1474px] absolute top-[1392px] bg-[#313131]'>
        <div className='w-[1120px] h-[1246px] absolute top-[100px] left-[123px]'>
            <div className='w-[1120px] h-[152px] gap-[525px] flex'>
                <div className='w-[455px] h-[152px] gap-[16px] '>
                    <div className='w-[184px] h-[24px] flex'>
                        <div className='absolute top-[11.1px]'><img src="line.svg"/></div>
                        <div className='w-[126px] h-[24px] font-inter text-[20px] font-bold leading-[24.2px] text-left bg-[079211] absolute left-[58px] top-[2px] text-[#079211]'>Recent Work</div>
                    </div>
                    <div className='w-[455px] h-[112px] font-inter text-[46px] font-bold leading-[56px] text-left text-[white] relative top-[16px] '>
                    Some of my favorite projects.
                    </div>
                </div>
                <div className='w-[140px] h-[38px] border-[1px] border-[#079211] py-[10px] px-[16px] absolute top-[57px] left-[980px]'>
                    <div className='w-[108px] h-[18px] font-inter text-[14px] font-bold leading-[18px] text-left text-white'>View All Project</div>
                </div>
            </div>
        </div>

        <div className='w-[1120px] h-[1004px] gap-[48px]  mt-[342px] ml-[123px] z-10 flex-col'>
            <div className='w-[1120px] h-[478px] gap-[20px] flex'>
                <div className='w-[550px] h-[478px] gap-[38px]'>
                    <img src="alana.svg"/>
                    <div className='w-[279px] h-[72px] mt-[38px]'>
                        <div className='w-[279px] h-[32px] font-inter text-[28px] font-bold leading-[32px] text-left text-white'>Alana - live data app</div>
                        <div className='w-[143px] h-[24px] gap-[10px] mt-[16px] flex'>
                            <div className='w-[109px] h-[24px] font-inter text-[20px] font-medium leading-[24.2px] text-left text-[#079211]'>See project</div>
                            <div className='w-[24px] h-[24px]'><img src="Vector1.svg" className='relative top-[3.75px] left-[3.75px]'/></div>
                        </div>
                    </div>
                </div>

                <div className='w-[550px] h-[478px] gap-[38px]'>
                    <img src="wohoo.svg"/>
                    <div className='w-[279px] h-[72px]  mt-[38px]'>
                        <div className='w-[294px] h-[32px] font-inter text-[28px] font-bold leading-[32px] text-left text-white'>Wohoo - weather app</div>
                        <div className='w-[143px] h-[24px] gap-[10px] mt-[16px] flex'>
                            <div className='w-[109px] h-[24px] font-inter text-[20px] font-medium leading-[24.2px] text-left text-[#079211]'>See project</div>
                            <div className='w-[24px] h-[24px]'><img src="Vector1.svg" className='relative top-[3.75px] left-[3.75px]'/></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className='w-[1120px] h-[478px] gap-[20px] flex'>
                <div className='w-[550px] h-[478px] gap-[38px]'>
                    <img src="pagee.svg"/>
                    <div className='w-[333px] h-[72px] mt-[38px]'>
                        <div className='w-full h-[32px] font-inter text-[28px] font-bold leading-[32px] text-left text-white'>Pagee - real estate UI kit</div>
                        <div className='w-[143px] h-[24px] gap-[10px] mt-[16px] flex'>
                            <div className='w-[109px] h-[24px] font-inter text-[20px] font-medium leading-[24.2px] text-left text-[#079211]'>See project</div>
                            <div className='w-[24px] h-[24px]'><img src="Vector1.svg" className='relative top-[3.75px] left-[3.75px]'/></div>
                        </div>
                    </div>
                </div>

                <div className='w-[550px] h-[478px] gap-[38px] '>
                    <img src="keikoko.svg"/>
                    <div className='w-[279px] h-[72px] mt-[38px]'>
                        <div className='w-[300px] h-[32px] font-inter text-[28px] font-bold leading-[32px] text-left text-white'>Keikoko - agency web</div>
                        <div className='w-[143px] h-[24px] gap-[10px] mt-[16px] flex'>
                            <div className='w-[109px] h-[24px] font-inter text-[20px] font-medium leading-[24.2px] text-left text-[#079211]'>See project</div>
                            <div className='w-[24px] h-[24px]'><img src="Vector1.svg" className='relative top-[3.75px] left-[3.75px]'/></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </>
  )
}

export default Work









