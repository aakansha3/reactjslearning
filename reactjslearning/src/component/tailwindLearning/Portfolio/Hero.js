import React from 'react'
import './Navbar.css'
import { TypeAnimation } from 'react-type-animation'
const Hero = () => {
  return (
    <>
    <div className='w-[769px] h-[394px] absolute top-[174px] left-[123px] gap-[58px]'>
        <div className='w-[751px] h-[282px] gap-[38px]'>
            <div className='w-[751px] h-[162px]' style={{ marginBottom: '38px' }}>
                <div className='w-[340px] h-[68px] leading-[68px] text-[38px] text-[#079211] font-inter font-bold'>Hello, I’m Alif Reza</div>
                <div className='w-[751px] h-[94px] font-inter font-bold text-white leading-[94px] text-[88px]'>
                <TypeAnimation sequence={[
                ' Designer',
                1000,
                'Web Developer',
                1000,
                'Consultant',
                1000,
            ]}
            wrapper='span'
            speed={50}
            repeat={Infinity}
            />
                    
                </div>
            </div>
            <div className='w-[550px] h-[82px] font-bold font-inter text-[16px] leading-[28px] text-[#808080]'>I’m from Singapore and I have been working as a Product Designer for more than 7 years. I’ve worked for a Hanziree company Pabloo as a Product Designer and Front-end Developer for 3 years.
            </div>
        </div>
        <div className='w-[328px] h-[54px] flex' style={{ marginTop: '58px' }}>
            <div className='w-[136px] h-[54px] py-[18px] px-[28px] gap-[10px] bg-[#079211]'>
                <div className='w-[80px] h-[18px] text-[18px] leading-[18px] font-inter font-bold text-white'>Email Me</div>
            </div>
            <div className='py-[18px] ml-[38px]'>
                <img src="Vector.svg"/>
            </div>
            <div className='w-[120px] h-[18px] font-inter font-bold text-[18px] leading-[18px] text-[#808080] py-[18px] ml-[10px]'>Download CV</div>
        </div>
    </div>
    </>
  )
}

export default Hero
