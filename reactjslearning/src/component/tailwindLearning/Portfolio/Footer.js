import React from 'react'
import './Navbar.css'

const Footer = () => {
  return (
    <div className='w-[1366px] h-[335px] absolute top-[3733px] bg-[#050507]'>

      <div className='w-[1120px] h-[258px] absolute top-[48px] left-[123px] gap-[68px] '>
        <div className='w-[978px] h-[147px] gap-[463px]  flex'>
            <div className='w-[107px] h-[28px] font-inter text-[26px] font-bold font-inter leading-[28px] text-left text-white '>Alifreza<span className='font-inter text-[26px] font-bold leading-[28px] text-left text-[#079211]'>.</span></div>
            <div className='w-[408px] h-[147px] gap-[114px]  flex absolute left-[570px]'>
                <div className='w-[171px] h-[147px] gap-[26px] '>
                    <div className='w-[112px] h-[24px] font-inter text-[20px] font-semibold leading-[24.2px] text-left text-white'>Information</div>
                    <div className='w-[171px] h-[97px] gap-[20px] flex flex-col mt-[26px]'>
                        <div className='w-[132px] h-[19px] font-inter text-[16px] font-normal leading-[19.36px] text-left text-[#808080]'>+65 8901376694</div>
                        <div className='w-[167px] h-[19px] font-inter text-[16px] font-normal leading-[19.36px] text-left text-[#808080]'>alifreza08@gmail.com</div>
                        <div className='w-[171px] h-[19px] font-inter text-[16px] font-normal leading-[19.36px] text-left text-[#808080]'>Changi Bay, Singapura</div>
                    </div>
                </div>

                <div className='w-[123px] h-[147px] gap-[26px] '>
                    <div className='w-[123px] h-[24px] font-inter text-[20px] font-semibold leading-[24.2px] text-left text-white'>Social</div>
                    <div className='w-[66px] h-[97px] gap-[20px] flex flex-col mt-[26px]'>
                        <div className='w-[65px] h-[19px] font-inter text-[16px] font-normal leading-[19.36px] text-left text-[#808080]'>Dribbble</div>
                        <div className='w-[66px] h-[19px] font-inter text-[16px] font-normal leading-[19.36px] text-left text-[#808080]'>Behance</div>
                        <div className='w-[63px] h-[19px] font-inter text-[16px] font-normal leading-[19.36px] text-left text-[#808080]'>Linkedin</div>
                    </div>
                </div>

            </div>
        </div>
      </div>

        <div className='w-[1120px] h-[43px] gap-[26px]  absolute left-[123px] top-[263px] border-t-[1px] border-[#313131]'>
            <div className='w-[1120px] h-[17px] gap-[798px]  absolute top-[26px] flex'>
                <div className='w-[205px] h-[17px] font-inter text-[14px] leading-[16.94px] text-left text-white'>Design and develop by <span className='font-inter text-[14px] font-normal leading-[16.94px] text-left text-[#079211]'>Alifreza</span></div>
                <div className='w-[117px] h-[17px] font-inter text-[14px] font-normal leading-[16.94px] text-left text-white'>Copyright ©2023</div>
            </div>
        </div>

    </div>
  )
}

export default Footer










