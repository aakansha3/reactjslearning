import React from 'react'

import { TypeAnimation } from 'react-type-animation'
const Hero = () => {
  return (
    <div className='grid grid-cols-1 sm:grid-cols-3 gap-8 max-w-[1200px] md:h-[70vh] mx-auto py-8 bg-black'>
        <div className='my-auto mx-auto w-[300px] h-auto lg:w-[400px]'>            
            <img src='logoperson.png' alt = "hero image"/>            
        </div>
        <div className='col-span-2 px-5 mt-[20px] sm:mt-[-100px] sm:mr-[30px] md:mt-[40px] xl:mr-[30px]'>
            <h1 className='text-white text-4xl sm:text-5xl lg:text-8xl font-extrabold mt-[100px]'>
            <span className='primary-color'>
                I'm a
            </span> <br/>
            <TypeAnimation sequence={[
                'Frontend Dev',
                1000,
                'webdesigner',
                1000,
                'Consultant',
                1000,
            ]}
            wrapper='span'
            speed={50}
            repeat={Infinity}
            />
            </h1>
            <p className='text-white sm:text-lg my-6 lg:text-xl'>
                My name is John Doe and I have 5+ year experience in web development.
            </p>
            <div className='my-[10px]'>
                <a href="/" className='px-6 py-3 w-full rounded-xl mr-4 bg-gradient-to-br from-orange-500 to-pink-500 text-white'>Download CV</a>
                <a href='/' className='px-6 py-3 w-full rounded-xl mr-4 border border-gray-400 hover:bg-gradient-to-br from-orange-500 to-pink-500 text-white hover:border-none'>Contact</a>
            </div>
        </div>
    </div>
  )
}

export default Hero
