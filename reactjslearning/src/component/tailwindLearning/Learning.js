import React from 'react'
import Navbar from './Navbar'
import Hero from './Hero'
import Skills from './Skills'
const Learning = () => {
    return (
        <>
        <div className='bg-black'>
        <Navbar/>
        <Hero/>
        <Skills/>
        </div>
        </>
    )
}

export default Learning
