import React from 'react'
const Skills = () => {
  return (
    <div className='bg-black text-gray-400 md:h-[150px] max-w-[1200px] mx-auto grid grid-cols-3 place-item-center md:flex md:justify-between md:items-center'>
        <h2 className='text-gray-700 text-2xl md:text-4xl font-bold m-4'>My<br/> Tech <br/> Stack</h2>  
        <div className='flex flex-col items-center m-4 sm:my-0 w-[80px] md:w-[100px]'>
        <img src="html.jpeg" alt="" className='h-[100px] w-[100px]'/>
        <p className='mt-2'>Html</p>    
        </div>
        <div className='flex flex-col items-center m-4 sm:my-0 w-[80px] md:w-[100px]'>
        <img src="css.jpeg" alt="" className='h-[100px] w-[100px]'/>
        <p className='mt-2'>CSS</p>    
        </div> 
        <div className='flex flex-col items-center m-4 sm:my-0 w-[80px] md:w-[100px]'>
        <img src="js.png" alt="" className='h-[100px] w-[100px]'/>
        <p className='mt-2'>Javascript</p>    
        </div>   
        <div className='flex flex-col items-center m-4 sm:my-0 w-[80px] md:w-[100px]'>
        <img src="laravel.png" alt="" className='h-[100px] w-[100px]'/>
        <p className='mt-2'>laravel</p>    
        </div>  
    </div>
  )
}

export default Skills
