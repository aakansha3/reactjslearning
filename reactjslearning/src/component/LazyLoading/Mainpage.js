import React,{Suspense, lazy} from 'react'
// import Home from './Home'
// import About from './About'
const Home = lazy(()=>import('./Home'))
const About = lazy(()=>import('./About'))
const Mainpage = () => {
  return (
    <div>
        <h1>Lazy Loading</h1>
      <Suspense fallback={<div>Loading...Please wait</div>}>
      <Home/>
      <About/>
      </Suspense>
    </div>
  )
}

export default Mainpage
