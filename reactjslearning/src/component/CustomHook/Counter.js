import React,{useState} from 'react'
import useCounter from './useCounter'
const Counter = () => {
    const [count,handleAdd,handleSub] = useCounter();
  return (
    <div>
      <h1>{count}</h1>
      <button onClick={handleAdd}>Increment</button>
      <button onClick={handleSub}>Decrement</button>
    </div>
  )
}

export default Counter
