// import React from 'react'
// import Child from './Child';
// const Parent = () => {
//   return (
//     <div>
//       <Child name="Yoshita"/>
//     </div>
//   )
// }

// export default Parent
// above code will pass data from parent to child using prop 

import React from 'react'
import Child from './Child';
const Parent = () => {
    function getData(data){
        console.log(data);
    }
  return (
    <div>
      <Child getData={getData}/>
    </div>
  )
}
export default Parent