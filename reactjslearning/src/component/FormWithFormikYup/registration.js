import React from "react";
import { useFormik } from 'formik';
import { signUpSchema } from "./schema";
const initialValues = {
    name:'',
    email:'',
    password:'',
    confirm_password:'',
}

const Registration = () => {
 
const {values,errors,touched,handleBlur,handleChange,handleSubmit} = useFormik({
    initialValues:initialValues,
    validationSchema:signUpSchema,
    onSubmit:(values,action)=>{
        console.log(values);
        action.resetForm();
    },
});
    return (
        <>

            <form onSubmit={handleSubmit}>
                <div>                   
                    <input
                        type="name"
                        autoComplete="off"
                        name="name"
                        id="name"
                        placeholder="Name"
                        values={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    {errors.name && touched.name ? (
                    <p>{errors.name}</p>)  : null}
                </div>
                <div className="input-block">                    
                    <input
                        type="email"
                        autoComplete="off"
                        name="email"
                        id="email"
                        placeholder="Email"
                        values={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    {errors.email && touched.email ? (
                    <p>{errors.email}</p>)  : null}
                </div>
                <div className="input-block">                   
                    <input
                        type="password"
                        autoComplete="off"
                        name="password"
                        id="password"
                        placeholder="Password"
                        values={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    {errors.password && touched.password ? (
                    <p>{errors.password}</p>)  : null}
                </div>
                <div className="input-block">                    
                    <input
                        type="password"
                        autoComplete="off"
                        name="confirm_password"
                        id="confirm_password"
                        placeholder="Confirm Password"
                        values={values.confirm_password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                    {errors.confirm_password && touched.confirm_password ? (
                    <p>{errors.confirm_password}</p>)  : null}
                </div>
                <button className="input-button" type="submit">
                    Registration
                </button>
            </form>





        </>
    );
};


export default Registration;