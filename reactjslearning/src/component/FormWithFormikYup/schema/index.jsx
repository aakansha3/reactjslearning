import * as Yup from 'yup';

export const signUpSchema = Yup.object({
    name:Yup.string().min(3).max(15).required("Please Enter your Name"),
    email:Yup.string().email().required('Please Enter your Email'),
    password:Yup.string().min(6).required('Please Enter Your Password'),
    confirm_password:Yup.string().required().oneOf([Yup.ref('password'),null], 'Password must match'),
});