import React, { useState } from 'react';
import './register.css';
import axios from 'axios';
const Register = () => {
    const [formData, setFormData] = useState({
        name: "",
        email: "",
        password: "",
        password_confirmation: "", // Corrected field name
    });

    function handleSubmit(e) {
        e.preventDefault();
        console.log(formData);        
        axios.post('http://laraveldoc.localhost/api/v1/auth/registeration', {
            name: formData.name,
            email: formData.email,
            password: formData.password,
            password_confirmation: formData.password_confirmation,
        },{
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json',
            }
        }).then(response=>{
            console.log(response.data);
        }).catch(error=>{
            console.error('Error:',error);
        });
    }    

    function handleChange(e) {
        const { name, value } = e.target;
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: value,
        }));
    }

    return (
        <>
        <div className='register'>        
        <div className='img'><img src="signup.webp"/></div>
        <div className='data'>
            <h1 className='signup'>Signup</h1>
            <form onSubmit={handleSubmit}>
                <div className='name'>
                    <img src="logoperson.png" className='logo'/>
                    <input className='input' type="text" name="name" value={formData.name} onChange={handleChange} placeholder="Your Name" />
                </div>
                <div className='email'>
                <img src="logomail.png" className='logo'/>
                    <input
                        type="email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                        placeholder="Your Email"
                    />
                </div>
                <div className='password'>
                <img src="lock.png" className='logo'/>
                    <input
                        type="password"
                        name="password"
                        value={formData.password}
                        onChange={handleChange}
                        placeholder="Password"
                    />
                </div>
                <div className='password'>
                <img src="logokey.jpeg" className='logo'/>
                    <input
                        type="password"
                        name="password_confirmation"
                        value={formData.password_confirmation} 
                        onChange={handleChange}  
                        placeholder="Repeat Your Password"
                    />
                </div>
                <button type="submit" className='submit'>Register</button>
            </form>
        </div>
        </div>
        </>
    );
}

export default Register;

