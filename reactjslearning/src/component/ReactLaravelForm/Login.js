import React,{useState} from 'react'
import "./register.css";
import axios from 'axios';
const Login = ({setIsLoggedIn}) => {
    const [formData, setFormData] = useState({       
        email: "",
        password: "",        
    });

    function handleSubmit(e) {
        e.preventDefault();
        console.log(formData);
        axios.post('http://laraveldoc.localhost/api/v1/auth/logination',{
            email: formData.email,
            password: formData.password,
        },{
            headers: { // Corrected field name
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
        }).then(response => {  
            const data = response.data;          
            console.log(response.data); 
            if(data.access_token){
                localStorage.setItem('token', data.access_token);
                localStorage.setItem('refresh_token',data.refresh_token);
                setIsLoggedIn(true);
                console.log(data);
            }
                      
        }).catch(error => {
            console.log('Error', error);
        });      
        
    }   
    function handleChange(e) {
        const { name, value } = e.target;
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: value,
        }));
    }
  return (
    <><div className='register'>        
    <div className='img'><img src="signup.webp"/></div>
    <div className='data'>
        <h1 className='signup'>Login</h1>
        <form onSubmit={handleSubmit}>            
            <div className='email'>
            <img src="logomail.png" className='logo'/>
                <input
                    type="email"
                    name="email"
                    value={formData.email}
                    onChange={handleChange}
                    placeholder="Your Email"
                />
            </div>
            <div className='password'>
            <img src="logokey.jpeg" className='logo'/>
                <input
                    type="password"
                    name="password"
                    value={formData.password}
                    onChange={handleChange}
                    placeholder="Password"
                />
            </div>            
            <button type="submit" className='submit'>Login</button>
        </form>
    </div>
    </div>
    </>
  )
}

export default Login
