import React,{useState,useEffect} from 'react';
import Navigation from './Navigation';
import axios from 'axios'; // Import Axios library
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from "./Login";
import Register from './Register';
import Profile from './Profile';
import Logout from './Logout';
import PrivateRoutes from './PrivateRoutes';
const Main = () => {
    const[isLoggedIn,setIsLoggedIn] = useState(false);

    useEffect(() => {
      const checkTokenExpiration = () => {
          const token = localStorage.getItem('token');
          if (token) {
              // Decode the token to extract expiration time
              const decodedToken = decodeToken(token);
              const expirationTime = decodedToken.exp * 1000; // Convert expiration time to milliseconds
              const currentTime = Date.now();

              if (currentTime >= expirationTime) {
                  // Token expired, perform logout
                  refreshToken();
              }
          }
      };

      // Check token expiration periodically
      const interval = setInterval(checkTokenExpiration, 60000); // Check every minute
      return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    // Check if token exists in localStorage to determine isLoggedIn state
    const token = localStorage.getItem('token');
    if (token) {
        setIsLoggedIn(true);
    } else {
        setIsLoggedIn(false);
    }
}, []);

    const decodeToken = (token) => {
      try {
          const base64Url = token.split('.')[1];
          const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
          const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
              return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          }).join(''));

          return JSON.parse(jsonPayload);
      } catch (error) {
          return null;
      }
  };  

const handleLogout = () => {
    localStorage.removeItem('token'); // Remove token from localStorage
    localStorage.removeItem('refresh_token');
    setIsLoggedIn(false); // Update isLoggedIn state to false
};

const refreshToken = async ()=> {
  try{
    const refreshToken = localStorage.getItem('refresh_token');
    const response = await axios.post('http://laraveldoc.localhost/api/v1/auth/refresh_access_token',{
      refresh_token : refreshToken
    });
    const { access_token } = response.data;
    localStorage.setItem('token',access_token);
  }
  catch(error){
    console.log('Failed to refresh token:',error);
    handleLogout();
  }
}

//     useEffect(() => {      
//       console.log('Logged In',isLoggedIn); 
//   },[isLoggedIn]);  
//   const handleLogout = () => {
//     localStorage.removeItem('token'); // Remove token from localStorage
//     setIsLoggedIn(false); // Update isLoggedIn state to false
// }; 

  return (
    <Router>   
      <Navigation isLoggedIn={isLoggedIn} handleLogout={handleLogout}/>
      <Routes>
        <Route path="/login" element={<Login setIsLoggedIn={setIsLoggedIn}/>} />
        <Route path="/register" element={<Register />} />
        <Route element={<PrivateRoutes/>}>
        <Route path="/profile" element={<Profile />} />   
        <Route path="/logout" element={<Logout/>} />  
        </Route>
      </Routes>
    </Router>  
  );
}

export default Main;

