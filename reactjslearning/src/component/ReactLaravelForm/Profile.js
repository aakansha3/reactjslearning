import React, { useEffect,useState } from 'react';

const Profile = () => {
    const [profileData,setProfileData] = useState(null);
    useEffect(() => {
        const token = localStorage.getItem('token');
        console.log(token);

        fetch('http://laraveldoc.localhost/api/v1/auth/profile', {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            return response.json();
        }).then(data => {
            setProfileData(data);
        }).catch(error => {
            console.error('Error:', error);
        });
    }, []); // Empty dependency array to run only once when component mounts

    return (
        <div>
            <h1>Profile</h1>
            {profileData && (
                <div>
                    <h1>Name:{profileData.name}</h1>
                    <h1>Email:{profileData.email}</h1>                    
                </div>
            )}
        </div>
        
    );
}

export default Profile;
