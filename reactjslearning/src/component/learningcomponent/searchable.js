import React from 'react'

const Searchable = () => {
  return (
    <div>
      <input type="text" placeholder="Search..."/><br/>
      <div>
      <input type="checkbox" id="stock" name="stock" value="Bike"/>
      <label htmlFor="stock" className='input'>Only show products in stock</label>
      </div>
    </div>
  )
}

export default Searchable
