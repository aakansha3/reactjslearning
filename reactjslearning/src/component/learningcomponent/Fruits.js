import React from 'react'
import Item from './api';
const Fruits = () => {
    const FruitsItems = Item.filter(item => item.category === "Fruits");
    
  return (
    <>
    <div>
      <label className='input'>Fruits</label>
      <table>     
      
      {FruitsItems.map((curElem)=>{
        return (
            <>
            <tr>
            <td className='input'>{curElem.name}</td>
            <td className='input'>{curElem.price}</td>
            </tr>
            </>
            )
      })}
      </table>
    </div>    
    </>
  )
}

export default Fruits
