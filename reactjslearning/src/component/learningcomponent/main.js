import React from 'react'
import Searchable from './searchable'
import Fruits from './Fruits'
import Vegetable from './Vegetable'
const Main = () => {
  return (
    <div>
      <Searchable/>
      <Fruits/>
      <Vegetable/>
    </div>
  )
}

export default Main
