import React,{useState,useEffect} from 'react';
import {useQuery} from 'react-query';
import axios from 'axios';
const ReactQuery = () => {
    const { isLoading, error ,data} = useQuery('dogs',()=>
        axios('https://random.dog/woof.json')
    );
    if(error) return<h1>Error:{error.message},try again!</h1>
    if(isLoading) return <h1>Loading...</h1>;
  return (
    <div>
      <img src={data.data.url}/>
    </div>
  )
}

export default ReactQuery
