import { create } from 'zustand';

const ZustandStore = create((set)=>({
    data:[],
    loading: true,
    favorites: [],
    query: '',
    language: '',
    category: '',
    fetchSuccess: (data) => set({ data, loading: false }),
    updateCategory: (category) => set({ category }),
    updateQuery: (query) => set({ query }),
    updateLanguage: (language) => set({ language }),
    toggleFavorites: (article) => {
        set((state) => {
            // Check if the article is already in favorites
            const isFavorite = state.favorites.some((fav) => fav.title === article.title);

            let updatedFavorites;
            if (isFavorite) {
                // If the article is already a favorite, remove it
                updatedFavorites = state.favorites.filter((fav) => fav.title !== article.title);
            } else {
                // If the article is not a favorite, add it
                updatedFavorites = [...state.favorites, article];
            }

            // Merge the updated favorites with the existing favorites from local storage
            const existingFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
            const mergedFavorites = [...existingFavorites, ...updatedFavorites];

            // Remove duplicates from the merged favorites array based on title
            const uniqueFavorites = mergedFavorites.filter((fav, index, self) =>
                index === self.findIndex((f) => f.title === fav.title)
            );

            // Update local storage with the unique favorites array
            localStorage.setItem('favorites', JSON.stringify(uniqueFavorites));

            // Return the updated state with unique favorites
            return { favorites: uniqueFavorites };
        });
    },
    setFavorites: (favorites) => set({ favorites }),
    removeFromFavorites: (articleToRemove) => {
        set((state) => {
            const updatedFavorites = state.favorites.filter((article) => article.title !== articleToRemove.title);            
            localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
            return { favorites: updatedFavorites };
        });
    }
}));

export default ZustandStore;
