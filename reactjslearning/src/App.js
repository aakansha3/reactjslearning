import './App.css';
import React from 'react';

// import Restaurant from "./component/Basics/Restaurant";
// import UseState from "./component/hooks/useState";
// import TextInput from "./component/hooks/TextInput";
// import Toggle from './component/hooks/Toggle';
// import useEffect from './component/hooks/useEffect';
// import UseEffect from './component/hooks/useEffect';
// import Board from './component/tictactoe';
// import Parent from './component/StateLifting/Parent';
// import Main from './component/learningcomponent/main';
// import UseRef from './component/hooks/useRef';
// import UseMemo from './component/hooks/useMemo';
// import Form from './component/form';
// import Parent from './component/hooks/useContext/Parent';
import Main from './component/ReactLaravelForm/Main';
import NewsApp from './component/NewsApp/NewsApp';
// import Mainpage from './component/LazyLoading/Mainpage';
// import UseReducer from './component/hooks/useReduser';
import Registration from './component/FormWithFormikYup/registration';
import Mainredux from './component/hooks/Redux/mainredux';
import Learning from './component/tailwindLearning/Learning';
import Counter from './component/CustomHook/Counter';
import Counter2 from './component/CustomHook/counter2';
import ReactQuery from './component/ReactQuery/ReactQuery';
import {QueryClient,QueryClientProvider} from 'react-query';
// import Main from './component/tailwindLearning/Portfolio/Main'


const queryClient = new QueryClient();
function App() {
  return (   
    <>
    {/* <MyName surname="karena"/>
    <MyName/>
    <h1>Hello World!!</h1>
    <h3>Awesome</h3> */}
    {/* <Restaurant/> */}
    {/* <UseState/> */}
    {/* <TextInput/> */}
    {/* <Toggle/> */}
    {/* <UseEffect/> */}
    {/* <Board/> */}
    {/* <Parent/> */}
    {/* <Main/> */}
    {/* <UseRef/> */}
    {/* <Parent/> */}
    {/* <UseMemo/> */}
    {/* <Form/> */}
    {/* <Main/> */}
    {/* <NewsApp/> */}
    {/* <Mainpage/> */}
    {/* <UseReducer/> */}
    {/* <Mainredux/> */}
    {/* <Registration/>    */}
    {/* <Learning/> */}
    {/* <Counter/>
    <Counter2/> */}
    {/* <QueryClientProvider client={queryClient}>
    <ReactQuery/>
    </QueryClientProvider> */}
    <Main/>
    
    </>
  );
}
// const MyName = (props) => {
//   return <h1>Aakansha {props.surname}</h1>;
// }

export default App;

